use std::{env::args, time::SystemTime};

fn is_prime(num: i32) -> bool {
    for i in 2..num {
        if num % i == 0 {
            return false;
        }
    }

    return true;
}

fn main() {
    let first = i32::from_str_radix(args().nth(1).expect("No first value").as_str(), 10).unwrap();
    let last = i32::from_str_radix(args().nth(2).expect("No last value").as_str(), 10).unwrap();

    let start_time = SystemTime::now();

    let mut result: Vec<i32> = Vec::new();
    for i in first..last {
        if is_prime(i) {
            result.push(i);
        }
    }

    let elapsed = start_time.elapsed().unwrap();
    println!("Found {} prime numbers in {}ms ({}ns)", result.len(), elapsed.as_millis(), elapsed.as_nanos());
    println!("Numbers: {}", Vec::from_iter(result.into_iter().map(|i| i.to_string())).join(", "));
}
